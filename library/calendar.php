<?php

class calendar extends instance
{

    private $event = array();
    private $select;
    protected $next;
    protected $prev;
    protected $calendar;

    public function __construct($select = false)
    {
        parent::__construct();
        $this->instance = get_instance();
        if ($select == false) {
            $this->select = date('Y-m');
        } else {
            $this->select = date('Y-m', strtotime($select));
        }

        $this->next = date('Y-m', (strtotime('next month', strtotime($this->select))));
        $this->prev = date('Y-m', (strtotime('previous month', strtotime($this->select))));
    }

    public function get_event($id)
    {
        $result = $this->instance->db->prepare('SELECT * FROM `calendar` where `id` = :id ');

        $result->bindParam(':id', $id);

        $result->execute();

        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function add_event($event)
    {
        $this->event[$event['date']] = $event;

        $result = $this->instance->db->prepare('INSERT INTO `calendar`(`date`, `title`, `description`) VALUES (:date,:title,:description)');

        $result->bindParam(':date', $event['date']);
        $result->bindParam(':title', $event['title']);
        $result->bindParam(':description', $event['description']);

        $result->execute();
    }

    private function load_event()
    {

        $result = $this->instance->db->query('SELECT * FROM `calendar`')->fetchall(PDO::FETCH_ASSOC);

        foreach ($result as $event) {
            $this->event[date('Y-m-d', strtotime($event['date']))][] = $event;
        }

    }

    protected function genarate()
    {

        $this->load_event();

        $calendar_array = array();

        $date = date('Y-m-d', strtotime($this->select));

        //จำนวนวันในเดือน จากตัวแปร date
        $number_day = date('t', strtotime($date));
        //ตัวแปรเก็บวัน
        $day = date('d', strtotime($date));
        //ตัวแปรเก็บเดือน
        $month = date('m', strtotime($date));

        $date_select = date('Y-m', strtotime($this->select));

        if (date('Y-m', strtotime($date)) != $date_select) {
            continue;
        }

        //ตัวแปรเก็บ ปี
        $year = date('Y', strtotime($date));

        $this->calendar['month'] = date('F', strtotime($date));
        $this->calendar['year']  = $year;

        $week = 0;

        for ($i = 1; $i <= $number_day; $i++) {
            //เก็บว่าวันที่ 1 เป็น วันจันทร์
            $tmp      = mktime(0, 0, 0, $month, $i, $year);
            $tmp_date = date('Y-m-d', $tmp);
            $dow      = date('w', $tmp);
            if ($dow == 0) {
                $week++;
            }
            $calendar_array[$week][$dow]['day']  = date('d', $tmp);
            $calendar_array[$week][$dow]['date'] = date('Y-m-d', $tmp);

            if (isset($this->event[$tmp_date])) {
                // เพิ่ม กิจกรรมใน อารเรย์ โดยใช้ index เป็นวันที
                $calendar_array[$week][$dow]['event'] = $this->event[$tmp_date];
            }

            if ($tmp_date == date('Y-m-d')) {
                $calendar_array[$week][$dow]['today'] = true;
            }

        }

        // ส่งค่ากลับไป
        return $calendar_array;
    }

}

class draw_calendar extends calendar
{

    public function draw()
    {

        $now_d = date('d');
        $now_m = date('F');

        $calendar = $this->genarate();
        //month year
        $html = '<div class="month"> <ul>     <li class="prev"><a href="./index.php?route=index&date=' . $this->prev . '">&#10094;</a></li>   <li class="next"><a href="./index.php?route=index&date=' . $this->next . '">&#10095;</a></li> <li style="margin-left:50px;">' . $this->calendar['month'] . ' [<a href="./index.php?route=add" style="color:#fff300 !important;">New Event</a>]<br> <span style="font-size:18px">' . $this->calendar['year'] . '</span> </li> </ul> </div>';

        //calendar header
        $html .= '<ul class="weekdays"> <li>Mo</li> <li>Tu</li> <li>We</li> <li>Th</li> <li>Fr</li> <li>Sa</li> <li>Su</li> </ul>';

        $html .= '<ul class="days">';
        foreach ($calendar as $week) {
            for ($dow = 0; $dow < 7; $dow++) {
                if (isset($week[$dow]['event'])) {
                    $html .= $this->event_element($week[$dow]);
                } else if (isset($week[$dow]['day'])) {
                    if (isset($week[$dow]['today'])) {
                        $html .= $this->active_element($week[$dow]);
                    } else {
                        $html .= $this->date_element($week[$dow]);
                    }
                } else {
                    $html .= $this->blank_element();
                }
            }
            $html .= '<br>';
        }
        $html .= '</ul>';

        return $html;
    }

    private function blank_element()
    {
        return '<li></li>';
    }

    private function date_element($day)
    {
        return '<li><a href="./index.php?route=add&date=' . $day['date'] . '" title="today">' . $day['day'] . '</a></li>';
    }

    private function active_element($day)
    {
        return '<li class="active" ><a href="./index.php?route=add&date=' . $day['date'] . '" title="today">' . $day['day'] . '</a></li>';
    }

    private function event_element($day)
    {
        $html = '<li class="event">';
            $html.= '<a href="./index.php?route=add&date=' . $day['date'] . '" >';
                $html.= $day['day'];
            $html.='</a>';
            $html.='<div class="list">';
            foreach ($day['event'] as $event) {
                $html.='<a href="./index.php?route=show&id=' . $event['id'] . '" >';
                $html.= '<h4>' . $event['title'] . '</h4>';
                $html.='</a>';
            }
            $html.='</div>';
        $html.='</li>';

        return $html;
    }
}
