<?php

require_once './config/database.php';
require_once './config/route.php';

require_once './library/instance.php';
require_once './library/input.php';

require_once './library/route.php';

require_once './library/database.php';
require_once './library/calendar.php';

require_once './helper/common.php';

$instance = new instance();

$db                 = new database($config['host'], $config['username'], $config['password'], $config['database'], $config['port']);
$instance->db       = $db->get();
$instance->calendar = new draw_calendar(input::get('date'));

$route = new route($config['route']);
$route->exec(input::get('route'));
